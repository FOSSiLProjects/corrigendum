# Corrigendum

A quick and easy way to generate tickets for Syndetics content errors in Polaris PowerPAC, Polaris Leap, or other ILS products.

![Corrigendum splash image](https://i.imgur.com/hmzNDSs.jpg)

### Same ticket, different data

When you're sending a ticket to Syndetics to correct a cover image or data in your PowerPAC or Polaris Leap install, chances are that the only difference between two tickets are the titles, authors, bibliographic control number, and so on. The bulk of the ticket is the same.

Corrigendum offers a simple way to generate a Syndetics error ticket by taking input and creating a ticket for you. Just fill out the form, click the button, and paste the ticket into the email.

### Instructions

![Corrigendum screenshot](https://i.imgur.com/EjCz7DU.png)

1. Fill out the form, providing the title, author, bibliographic control number, problem type, and a brief description of the problem.
2. Click the Create Email button. Corrigendum generates the ticket and saves it on the clipboard.
3. Go to your email and hit CTRL V. Your ticket will appear in your email.

### Requires

* AutoHotkey
* Windows

### Questions?

cyberpunklibrarian (at) protonmail (dot) com