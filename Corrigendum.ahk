#SingleInstance Force ;There can be only one.
SetWorkingDir %A_ScriptDir% ;Home is where the script lives

Gui, Font, s14 bold, Arial
Gui Add, Text,, Corrigendum - Report a Syndetics error
Gui, Font, s10 bold, Arial
Gui, Add, Text,, Title
Gui, Font, s10 norm, Arial
Gui, Add, Edit, vTitle w300
Gui, Font, s10 bold, Arial
Gui, Add, Text,, Author/Director
Gui, Font, s10 norm, Arial
Gui, Add, Edit, vAuthor w300
Gui, Font, s10 bold, Arial
Gui, Add, Text,, Bib Control Number
Gui, Font, s10 norm, Arial
Gui, Add, Edit, vBibControl w300
Gui, Font, s10 bold, Arial
Gui, Add, Text,, What is the problem you're reporting?
Gui, Font, s10 norm, Arial
Gui, Add, DropDownList, vProblemType w250, Wrong cover image||No cover image|Wrong data (summary, reviews, etc)|Something else (see comments below)
Gui, Font, s10 bold, Arial
Gui, Add, Text,, Brief description of the problem
Gui, Font, s10 norm, Arial
Gui, Add, Edit, h100 w450 vDescription
Gui, Add, Button, gCreateRpt, &Create Email ;Takes the information and builds the message.
Gui, Show, x1 y1 h500 w550, Corrigendum - Report a Syndetics error
Return

CreateRpt:
{
Gui, Submit, NoHide
MsgBox,0,Message ready, Message ready to paste into email.

Greeting := "Hello!"

Body := "We have an issue with the data on a title in our system. Information about the issue and title follows:"

RptTitle := "Title:"
RptAuthor := "Author/Director:"
RptIssue := "Issue:"
RptLink := "Link: https://mcldaz.org/view.aspx?cn="
RptURL := RptLink . BibControl

Closing := "Thank you so much!"

RptMsg = %Greeting% `n`n %Body% `n`n %RptTitle% %Title% `n %RptAuthor% %Author% `n %RptIssue% %ProblemType% `n %RptURL% `n`n %Description% `n`n %Closing%

Clipboard := RptMsg
Reload
}

GuiClose:
ExitApp